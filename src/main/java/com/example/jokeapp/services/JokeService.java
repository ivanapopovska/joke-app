package com.example.jokeapp.services;

import org.springframework.stereotype.Service;


public interface JokeService {
    String getJoke();
}
